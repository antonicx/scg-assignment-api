import * as express from 'express'
import * as controller from '../controllers/DOSCGController'

export const router = express.Router()
router.post('/findnumber', controller.findNumber)
router.get('/findvalue/:number', controller.findValue)
