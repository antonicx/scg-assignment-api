import * as express from 'express'
import * as controller from '../controllers/DOSCGController'

export const router = express.Router()
router.post('/webhook', controller.lineBot)
